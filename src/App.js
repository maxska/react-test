import './App.css';
import dsektionenlogo from './styles/images/dsektionen.png';
import ysektionenlogo from './styles/images/ysektionen.png';
import './styles/styles.css';
import './styles/navbar-style.css';
import { Icon } from '@iconify/react';
import Events from './Events.js';
import Navbar from './Navbar.js';

let countDownDate = new Date('Nov 18, 2021 10:00').getTime();
let x = setInterval(() => {
  let now = new Date().getTime();
  let distance = countDownDate - now;

  let days = Math.floor(distance / (1000 * 60 * 60 * 24));
  let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  let seconds = Math.floor((distance % (1000 * 60)) / 1000);

  document.getElementById(
    'hero-countdown'
  ).innerHTML = `${days} dagar | ${hours} timmar | ${minutes} min | ${seconds} sek`;

  if (distance < 0) {
    clearInterval(x);
    document.getElementById('hero-countdown').innerHTML = '';
  }
}, 1000);

const App = () => {
  return (
    <>
      <Navbar />

      <div class='hero' id='home'>
        <div class='hero-container'>
          <h1 class='hero-heading'>
            Arbetsmarknadsdagarna inom data och IT vid Linköpings universitet
          </h1>
          <h2 class='hero-subheading'>18 november 2021</h2>
          <div id='hero-countdown'></div>
        </div>
        <div class='before-about' id='about'></div>
      </div>

      <div class='about'>
        <div class='about-container' style={{
          maxWidth: "800px"
        }}>
          <h1>Välkommen till LINK-dagarna</h1>
          <p>
            LINK-dagarna är en arbetsmarknadsmässa på Linköping universitet som
            hålls för att olika företag ska kunna möta studenter som studerar
            data och IT inom sektionerna D, Y, I, SAKS, KOGV, Ling och StatLin.
          </p>

          <p>
            På LINK-mässan bjuds företag in som får ställa upp en monter och
            vara närvarande med företagsrepresentanter. Här får företagen och
            studenterna en möjlighet att träffas och interagera på ett naturligt
            sätt genom att studenterna går runt och minglar med företagen.
          </p>

          <p>
            På mässan äger även flera spännande seminarium, coffe meetings och
            case-kvällar rum. Här bjuder LINK-gruppen in representanter från
            företag som då på ett mer personligt och avslappnat sätt kan lära
            känna studenterna och presentera företaget.
          </p>

          <p>
            Slutligen som kommer även flera stora tävlingar hållas i samband med
            mässan där riktigt schyssta priser delas ut, vilket ni absolut inte
            vill missa.
          </p>

          <p>Hoppas vi ses i höst,</p>
          <p id='signoff'>Hälsningar LINK-gruppen</p>
        </div>
      </div>
      <div>
        <section class='companies' id='companies'>
          <div class='after-about'></div>
          <div class='companies-container'>
            <h2 id='companies-header'>
              Vill ditt företag ställa ut på mässan? Anmäl ert deltagande senast
              28/10 för en garanterad plats på mässan!
            </h2>
            <div class='companies-btn'>
              <a
                href='https://podio.com/webforms/25580175/1901504'
                class='button'
                id='company-application'
                target='_blank'
                rel='noopener noreferrer'
              >
                Anmälan utställare
              </a>
            </div>
          </div>
          <div class='before-socials'></div>
        </section>

        <div class='socials' id='contact'>
          <div class='socials-container'>
            <div class='socials-links-container'>
              <a
                href='https://www.facebook.com/linkdagarna'
                target='_blank'
                rel='noopener noreferrer'
                class='socials-links'
              >
                <Icon icon='bx:bxl-facebook' />
              </a>
              <a
                href='https://www.instagram.com/linkdagarna/'
                target='_blank'
                rel='noopener noreferrer'
                class='socials-links'
              >
                <Icon icon='mdi-instagram' />
              </a>
              <a
                href='https://www.linkedin.com/company/linkdagarna'
                target='_blank'
                rel='noopener noreferrer'
                class='socials-links'
              >
                <Icon icon='bx:bxl-linkedin' />
              </a>
            </div>
            <h2>
              {' '}
              Följ oss på våra sociala medier för uppdateringar och mer info!
            </h2>
            
            {/* <Events /> */}
          </div>
        </div>

        <section class='contacting'>
          <div class='after-socials'></div>

          <div class='contact-container'>
            <div class='mail-container'>
              <h1 class='contact-heading'>Kontakt</h1>
              <p>
                Allmäna frågor:{' '}
                <a href='mailto:projektledare@linkdagarna.se' class='mail'>
                  projektledare@linkdagarna.se
                </a>
              </p>
              <p>
                Webmaster:{' '}
                <a href='mailto:webmaster@linkdagarna.se' class='mail'>
                  it@linkdagarna.se
                </a>{' '}
              </p>
            </div>
            
            <div id='organizers'>
              <h1 class='organizer-heading'>Organisatörer</h1>
              <p>
                LINK arbetsmarknadsdagar anordnas av Y-sektionen och
                Datateknologsektionen Linköpingsuniversitet
              </p>
              <div class='orglogos-container'>
                <a href='https://d-sektionen.se/'>
                  <img
                    src={dsektionenlogo}
                    alt='D-sektionens logga'
                    class='orglogo'
                  />
                </a>
                <a href='https://ysektionen.se/'>
                  <img
                    src={ysektionenlogo}
                    alt='Y-sektionens logga'
                    class='orglogo'
                  />
                </a>
              </div>
            </div>
          </div>
        </section>

        <footer>
          <span> © 2021 LINK arbetsmarknadsdagar </span>
        </footer>
        <script src='main.js'></script>
      </div>
    </>
  );
};

export default App;
