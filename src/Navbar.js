import React, { useState } from 'react';
import linkdagarnalogo from './styles/images/logo.png';
import './styles/navbar-style.css';

const Navbar = () => {
  const [mobileMenuActive, setMobileMenuActive] = useState(false);
  const [menuLinksActive, setMenuLinksActive] = useState(false);

  return (
    <nav className='navbar'>
      <div className='navbar-container'>
        <a href='#home' id='navbar-logo'>
          <img src={linkdagarnalogo} alt='LINK-dagarna' id='logo'></img>
        </a>

        <div
          className={mobileMenuActive ? 'navbar-toggle is-active' : 'navbar-toggle'}
          id='mobile-menu'
          onClick={() => {
            setMobileMenuActive(!mobileMenuActive);
            setMenuLinksActive(!menuLinksActive);
          }}
        >
          <span className='bar'></span>
          <span className='bar'></span>
          <span className='bar'></span>
        </div>

        <ul className={menuLinksActive ? 'navbar-menu active' : 'navbar-menu'}>
          <li className='navbar-item'>
            <a href='#about' className='navbar-links' id='about-page'>
              Om LINK
            </a>
          </li>
          <li className='navbar-item'>
            <a href='#companies' className='navbar-links' id='company-page'>
              För företag
            </a>
          </li>
          <li className='navbar-item'>
            <a href='#contact' className='navbar-links' id='contact-page'>
              Kontakt
            </a>
          </li>

          <li className='apply-btn'>
            <a
              href='https://fair.linkdagarna.se'
              className='button'
              id='company-application'
              target='_blank'
              rel='noopener noreferrer'
            >
              Till Mässan
            </a>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
