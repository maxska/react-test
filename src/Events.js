import React, { useEffect } from 'react';
import axios from 'axios';

const ICS_URL = ''

const Events = (props) => {
  useEffect(() => {
    axios
      .get(
        ICS_URL
      )
      .then(
        (response) => {
          console.log(response);
        },
        (error) => {
          console.log(error);
        }
      );
  }, []);

  return (
    <div
      style={{
        width: '500px',
        height: '500px',
        backgroundColor: 'black',
        marginTop: '100px',
      }}
    >
      <div
        style={{
          width: '450px',
          height: '200px',
          backgroundColor: 'white',
          margin: '25px',
        }}
      >
        <div class='event'>
          <p
            style={{
              color: 'black',
              fontSize: '20px',
              width: '100%',
            }}
          ></p>
          <div
            style={{
              color: 'black',
              width: '100%',
            }}
          >
            <div
              style={{
                backgroundColor: 'pink',
                width: '100px',
                height: '100px',
                margin: '5px',
                color: 'black',
              }}
            ></div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Events;
